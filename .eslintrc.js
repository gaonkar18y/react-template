module.exports = {
  extends: [],
  plugins: ['react'],
  parser: 'babel-eslint',
  rules: {
    'max-len': [2, 120],
  },
  globals: {
    "window": true,
    "sessionStorage": true,
    "document": true,
    "alert": true,
    "navigator":true,
    "FormData":true
  }
};
