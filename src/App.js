import 'babel-polyfill';
import objAssign from 'core-js/fn/object/assign';
import promise from 'core-js/fn/promise';
import map from 'core-js/fn/map';
import set from 'core-js/fn/set';
import React from 'react';
import { Provider, connect } from 'react-redux';
import createHashHistory from 'history/createHashHistory';
import { store } from './redux/store/store';

import { registerReqResInterceptors } from './api/axios_client';
import { authActions } from './constants';
import { loadingActions } from './redux';

import Content from './containers/Contents'

const history = createHashHistory();

class App extends React.Component {
 
  componentDidMount() {


    const apiErrorHandler = (error) => {
      if (error.status === 401) {
        sessionStorage.clear();
        history.replace({ pathname: '/' });
        this.props.dispatch({ type: authActions.userLogout });
        return null;
      }
      if (error.config.method === 'post' || error.config.method === 'put') {
        let message = 'Could not save details due to technical error';
        if (error.config.customparams.notificationConf.validErrorResponses &&
          error.config.customparams.notificationConf.validErrorResponses.indexOf(error.response.data) !== -1) {
          message = error.response.data;
        } else if (error.config.customparams.notificationConf) {
          message = error.config.customparams.notificationConf.ErrorMessage;
        }
        loadingActions.showNotificationMessage(message, 'error', 4000);
      } else if (error.config.method === 'delete') {
        let message = 'Could not delete due to technical error';
        if (error.config.customparams.notificationConf) {
          message = error.config.customparams.notificationConf.ErrorMessage;
        }
        loadingActions.showNotificationMessage(message, 'error', 4000);
      } else if (error.config.method === 'get') {
        if (error.config.customparams.notificationConf) {
          loadingActions.showNotificationMessage(error.config.customparams.notificationConf.ErrorMessage,
            'error', 4000);
        }
      }
    };

    const apiSuccessHandler = (response) => {
      if (response.config.method !== 'get' && response.config.customparams.notificationConf) {
        loadingActions.showNotificationMessage(response.config.customparams.notificationConf.SuccessMessage,
          'success', 4000);
      }
    };
    registerReqResInterceptors(apiErrorHandler, apiSuccessHandler);
  }

  componentDidUpdate(prevProps, prevState) {

  }

  render() {
    return (
      <div>
        <Content history={history} />
      </div>
    );
  }
}

const mapStateToProps = state => ({ authStore: state.authReducer });

export default connect(mapStateToProps)(
  App
);
