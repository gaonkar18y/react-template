import { CancelToken, isCancel, create as createClient } from 'axios';
import createHashHistory from 'history/createHashHistory';
import { loadingActions } from '../redux';
import { contextPath } from '../constants';

const history = createHashHistory();

const client = createClient({
  timeout: 60000,
  baseURL: contextPath,
  headers: {
    common: {
      'Cache-Control': 'no-cache, no-store, must-revalidate',
      Pragma: 'no-cache',
    },
  },
});
let requestsCount = 0;

history.listen(() => {
  requestsCount = 0;
  loadingActions.hideLoadingSymbol();
});

const afterResponse = (response) => {
  if (response.config.customparams.showLoading) {
    requestsCount--;
  }
  if (requestsCount <= 0) {
    requestsCount = 0;
    loadingActions.hideLoadingSymbol();
  }
};
export default client;
export const registerReqResInterceptors = (errorAction, successAction) => {
  client.interceptors.request.use(
    config => {
      if (config.customparams.showLoading) {
        requestsCount++;
        loadingActions.showLoadingSymbol();
      }
      config.headers['Authorization'] = sessionStorage.getItem('authToken');
      return config;
    },
    error => {
      if (!isCancel(error)) {
        errorAction(error);
        throw error;
      }
    },
  );

  client.interceptors.response.use(
    response => {
      afterResponse(response);
      successAction(response);
      return response;
    },
    error => {
      afterResponse(error);
      if (!isCancel(error)) {
        errorAction(error);
        throw error;
      }
    },
  );
};

/**
 * Wraps promise to provide ability to cancel it.
 * Wrapper should be used for promises, which are resolved via => ...fetch(get('dataPath'))
 * But not via Promise.resolve() way.
 * @param  {Object|Promise} promise
 * @param  {Function} dataPathGetter
 * @return {Object|Promise}
 */
export const makeCancelable = (promise, dataPathGetter) => {
  const cancelablePromise = new Promise((resolve, reject) => {
    promise
      .then(dataPathGetter)
      .catch(reject)
      .then(resolve);
  });
  cancelablePromise.cancel = promise.cancel;
  cancelablePromise.isCancelled = promise.isCancelled;
  return cancelablePromise;
};
