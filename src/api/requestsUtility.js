import client from './axios_client';

export const putRequest = (url, data, notificationConf) => client.put(url, data,
  { customparams: { showLoading: true, notificationConf } });

export const postRequest = (url, data, notificationConf) => client.post(url, data,
  { customparams: { showLoading: true, notificationConf } });

export const getRequest = (url, isShowLoading, notificationConf) => client.get(url,
  { customparams: { showLoading: isShowLoading, notificationConf } });

export const deleteRequest = (url, notificationConf) => client.delete(url,
  { customparams: { showLoading: true, notificationConf } });
export const multipartRequest = (url, data, notificationConf) => client.post(url, data,
  { headers: {
    'Pragma': 'no-cache',
    'Cache-Control': 'no-cache,must-revalidate',
    'Content-Type': 'multipart/form-data',
  },
  timeout: 0,
  customparams: { showLoading: true, notificationConf } });

export const advDeleteRequest = (url, data, notificationConf) => client({ method: 'delete',
  url,
  customparams: { showLoading: true, notificationConf },
  params: { ...data },
});

export const deleteRequestWithBody = (url, data, notificationConf) => client({ method: 'delete',
  headers: { 'Pragma': 'no-cache', 'Cache-Control': 'no-cache, must-revalidate' },
  url,
  data,
  customparams: { showLoading: true, notificationConf },
});

export const callAdvPutRequest = (url, data, notificationConf) => client({ method: 'put',
  headers: {
    'Pragma': 'no-cache',
    'Accept': 'application/json',
    'Content-Type': 'application/json' },
  url,
  params: { imageId: data.imageId, imageLink: data.imageLink },
  customparams: { showLoading: true, notificationConf },
});
