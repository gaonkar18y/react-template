import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './AsynchComponent.scss';

export default class AsyncComponent extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      Component: null,
    };
  }

  componentDidMount() {
    this.loadComponent();
  }
  componentDidUpdate() {
    this.loadComponent();
  }

loadComponent = () => {
  if (!this.state.Component) {
    if (this.props.moduleProvider) {
      this.props.moduleProvider().then(module => {
        this.setState({ Component: module.default });
      });
    }
  }
}
render() {
  const { Component } = this.state;
  const spinnerStyle = { 'display': 'inline-block' };
  const defaultContent = this.props.hideLoading ? null :
    <div className="asynch-comp-loader-holder" style={this.props.customStyles}>
      Add your loader
    </div>;
  return (
    <div>
      { Component ? <Component {...this.props} /> : defaultContent}
    </div>
  );
}
}

AsyncComponent.propTypes = {
  customStyles: PropTypes.object,
  moduleProvider: PropTypes.func,
};

AsyncComponent.defaultProps = {
  customStyles: {},
};
