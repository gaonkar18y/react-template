import React from 'react';
import './Loader.scss';


class Loader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaderPaddingTop: this.getLoaderPaddingTop(),
      loaderContainerWidth: this.getLoaderContainerWidth(),
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateLoaderDimentions);
    // document.body.style.overflow = 'hidden';
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateLoaderDimentions);
    // document.body.style.overflow = 'auto';
  }

  updateLoaderDimentions = () => {
    this.setState({
      loaderPaddingTop: this.getLoaderPaddingTop(),
      loaderContainerWidth: this.getLoaderContainerWidth() });
  }

getLoaderPaddingTop = () => window.innerHeight / 2 - 30 - 56;

getLoaderContainerWidth = () => window.innerWidth - 184;

render() {
  const spinnerStyle = { 'display': 'inline-block' };
  let loaderContainerStyles = {
    paddingTop: this.state.loaderPaddingTop,
    width: this.state.loaderContainerWidth };
  if (this.props.uiUtils.loaderStyles) {
    loaderContainerStyles = { ...loaderContainerStyles, ...this.props.uiUtils.loaderStyles };
  }
  return (<div style={loaderContainerStyles} className="loading-spinner-holder">
    <div id="loader-inner-content">
      add your loader
      <h4></h4>
    </div>
  </div>);
}
}

export default Loader;

