import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { style } from './NotificationMessages.scss';

const NotificationMessages = (props) => (
  <div className="notificationMessage">
    Add Your notification message component
  </div>
);

NotificationMessages.propTypes = {
  kind: PropTypes.string,
  message: PropTypes.string,
};

export default NotificationMessages;
