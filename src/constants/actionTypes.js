export const dashboardActionTypes = {
  getUserInfo: 'GET_USER_INFO_ON_DASHBOARD',
};

export const authActions = {
  userLogout: 'ACTION_USER_LOGOUT',
  userLogin: 'ACTION_USER_LOGIN',
};

export const uiUtilsActions = {
  showLoading: 'ACTION_SHOW_LOADING',
  hideLoading: 'ACTION_HIDE_LOADING',
  showNotification: 'ACTION_SHOW_NOTIFICATION',
  hideNotification: 'ACTION_HIDE_NOTIFICATION',
};
