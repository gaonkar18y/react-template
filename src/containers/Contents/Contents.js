import React from 'react';
import { connect } from 'react-redux';
import { uiUtilsActions } from '../../constants';
import { AsyncComponent, Loader, ErrorBoundary, NotificationMessages }
  from '../../components/core';
import './Contents.scss';

const PreLoginRoutingHolder = () => import(/* webpackChunkName: "Login" */ '../preLoginRoutingHolder');
const RoutingHolder = () => import(/* webpackChunkName: "RoutingHolder" */ '../RoutingHolder');

class Content extends React.Component {
  constructor(props) {
    super(props);

  }

componentDidMount() {

}

componentWillUnmount() {
 
}


render() {
  let RoutingHolderComp = null;
  let loadingSpinnerContent = null;
  if (this.props.uiUtils.isLoading === true) {
    loadingSpinnerContent = <Loader {...this.props} {...this.state} />;
  }
  let notificatonContent = null;
  if (this.props.uiUtils.notificationConf.showNotification === true &&
    this.props.uiUtils.notificationConf.message) {
    notificatonContent = <NotificationMessages message={this.props.uiUtils.notificationConf.message}
      kind={this.props.uiUtils.notificationConf.kind}/>;
  }
  if (this.props.authStore.isLoggedIn) {
    RoutingHolderComp = RoutingHolder;
  
    return (<div>
          {loadingSpinnerContent} {notificatonContent}
          <ErrorBoundary>
            <AsyncComponent history={this.props.history} isLoading={this.props.uiUtils.isLoading}
              moduleProvider={RoutingHolderComp} />
          </ErrorBoundary>
        </div>
    );
  }
  return (
    <AsyncComponent history={this.props.history} moduleProvider={PreLoginRoutingHolder} />
  );
}
}

const mapStateToProps = state => ({
  authStore: state.authReducer,
  uiUtils: state.uiUtilsReducer,
});

export default connect(mapStateToProps)(
  Content
);
