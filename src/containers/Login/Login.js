import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { authActions, uiUtilsActions } from '../../constants';

class Login extends React.Component {
  constructor(props) {
    super(props);
    
  }
  loginUser = () => {
    console.log('Login user');
    this.props.dispatch({ type: authActions.userLogin });
  }
render() {
  return (
    <div>
       <p>Login page</p>
       <input type='button' value='Login' onClick={this.loginUser}/>
    </div>
    
  );
}
}
const mapStateToProps = state => ({
  authStore: state.authReducer,
});

Login.propTypes = {
  history: PropTypes.object,
  dispatch: PropTypes.func,
};

export default connect(mapStateToProps)(
  Login
);
