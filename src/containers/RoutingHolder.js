import React from 'react';
import { BrowserRouter, Switch, Route, Link, Router } from 'react-router-dom';

import { AsyncComponent } from '../components/core';

const Dashboard = () => import(/* webpackChunkName: "Dashboard" */ './Dashboard/Dashboard');

class RoutingHolder extends React.Component {
  

  render() {
    return (<div>
      <Router history={this.props.history}>
        <Switch>
          <Route path='/' exact={true}
            component={() => <AsyncComponent history={this.props.history} moduleProvider={Dashboard} />} />
        </Switch>
      </Router>
    </div>
    );
  }
}

export default RoutingHolder;
