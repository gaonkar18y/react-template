import React from 'react';
import { BrowserRouter, Switch, Route, Link, Router } from 'react-router-dom';

import { AsyncComponent } from '../components/core';

const Login = () => import(/* webpackChunkName: "Login" */ './Login/Login');

class PreLoginRoutingHolder extends React.Component {
  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.isLoading !== nextProps.isLoading) {
      return false;
    }
    return nextProps.isOpen === this.props.isOpen;
  }

  render() {
    return (<div>
      <Router history={this.props.history}>
        <Switch>
          <Route path='/' exact={true} component={() => <AsyncComponent
            history={this.props.history} moduleProvider={Login} />} />
          <Route path='/login' exact={true} component={() => <AsyncComponent
            history={this.props.history} moduleProvider={Login} />} />

        </Switch>
      </Router>
    </div>
    );
  }
}

export default PreLoginRoutingHolder;
