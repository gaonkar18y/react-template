import 'babel-polyfill';
import objAssign from 'core-js/fn/object/assign';
import promise from 'core-js/fn/promise';
import map from 'core-js/fn/map';
import set from 'core-js/fn/set';
import React from 'react';
import ReactDom from 'react-dom';
import { Provider, connect } from 'react-redux';
import store from './redux/store/store';
import App from './App';
import { ErrorBoundary } from './components/core';

ReactDom.render(
  <Provider store={store}>
    <ErrorBoundary>
      <App />
    </ErrorBoundary>
  </Provider>,
  document.getElementById('app')
);
