import { dashboardActionTypes } from '../../constants';
import { getDashboardInfo, getQuoteCounts, getTotalQuotes, getTotalUsers,
  getTotalLeads, getDashboardReports, getSavedReportsCount } from '../../api';


export const getUserInfoOnDashboard = (userType, userId) => (dispatch) => {
  getDashboardInfo(userId, userType).then((response) => {
    dispatch({ type: dashboardActionTypes.getUserInfoOnDashboard, payload: response.data });
  });
};
