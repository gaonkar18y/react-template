import { bindActionCreators } from 'redux';
import store from '../store/store';
import { uiUtilsActions } from '../../constants';

const showLoadingSymbol = (message) => (dispatch, getState) => {
  const currentState = getState();
  if (!currentState.uiUtilsReducer.isLoading) {
    dispatch({ type: uiUtilsActions.showLoading });
  }
};

const hideLoadingSymbol = (message) => (dispatch, getState) => {
  const currentState = getState();
  if (currentState.uiUtilsReducer.isLoading) {
    dispatch({ type: uiUtilsActions.hideLoading });
  }
};

const showNotificationMessage = (message, kind, timeout) => (dispatch, getState) => {
  dispatch({ type: uiUtilsActions.showNotification,
    payload: {
      message, kind,
    } });
  const notificationTimeOut = timeout || 5000;
  setTimeout(() => {
    dispatch({ type: uiUtilsActions.hideNotification });
  }, notificationTimeOut);
};

const loadingActions = bindActionCreators(
  {
    showLoadingSymbol,
    hideLoadingSymbol,
    showNotificationMessage,
  },
  store.dispatch
);

export default loadingActions;
