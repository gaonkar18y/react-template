import { authActions } from '../../constants';

const authReducer = (state = {
  isLoggedIn: null,
}, action) => {
  switch (action.type) {
    case authActions.userLogout:
      return { ...state, isLoggedIn: false };
    case authActions.userLogin:
      return { ...state, isLoggedIn: true };
    default: return state;
  }
};
export default authReducer;
