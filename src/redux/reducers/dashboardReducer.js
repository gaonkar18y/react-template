import { dashboardActionTypes } from '../../constants';

const dashboardReducer = (state = {
  userInfo: {},
}, action) => {
  const newState = Object.assign({}, state);
  switch (action.type) {
    case dashboardActionTypes.getUserInfoOnDashboard :
      newState.userInfo = action.payload;
      return newState;
    default:
  }

  return state;
};
export default dashboardReducer;
