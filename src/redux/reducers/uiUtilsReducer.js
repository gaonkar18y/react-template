import { uiUtilsActions } from '../../constants';

const uiUtilsReducer = (state = {
  isLoading: false,
  loaderStyles: null,
  notificationConf: {
    showNotification: false,
    message: '',
    kind: '',
  },
}, action) => {
  const newState = Object.assign({}, state);
  switch (action.type) {
    case uiUtilsActions.showLoading:
      newState.isLoading = true;
      newState.loaderStyles = action.payload;
      return newState;
    case uiUtilsActions.hideLoading :
      newState.isLoading = false;
      return newState;
    case uiUtilsActions.showNotification :
      newState.notificationConf.showNotification = true;
      newState.notificationConf.message = action.payload.message;
      newState.notificationConf.kind = action.payload.kind;
      return newState;
    case uiUtilsActions.hideNotification :
      newState.notificationConf = {
        showNotification: false,
        message: '',
        kind: '',
      };
      return newState;
    default: return state;
  }
};
export default uiUtilsReducer;
